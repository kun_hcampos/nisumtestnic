# Nisum Test RestAPI Usuarios

Rest API: Autenticacion JWT y Gestión de usuarios.

# Frameworks

- [Swagger ](https://swagger.io/docs/specification/about/) - es un lenguaje de descripción de interfaz para describir las API RESTful expresadas mediante JSON. Swagger se usa junto con un conjunto de herramientas de software de código abierto para diseñar, construir, documentar y usar servicios web RESTful. Swagger incluye documentación automatizada, generación de código y casos de prueba.
- [Spring Boot](https://spring.io/projects/spring-boot) - Spring Boot facilita la creación de aplicaciones independientes basadas en Spring de grado de producción que puede "simplemente ejecutar".
- [JWT](https://github.com/jwtk/jjwt) - JSON Web Tokens (JWT).
- [Spring Validation](https://docs.spring.io/spring-framework/docs/4.1.x/spring-framework-reference/html/validation.html#validation-beanvalidation) - Spring Validation facilita la validacion de entradad de datos.
- [Spring Data JPA](https://spring.io/projects/spring-data-jpa) - Spring Data JPA, parte de la familia Spring Data más grande, facilita la implementación de repositorios basados ​​en JPA que en su defecto usa la implementacion de Hibernate.
- [Spring Boot Starter Web](https://spring.io/projects/spring-boot) - Es todo lo necesario para poder arrancar un proyecto web api en minutos.
- [H2 Database](https://www.h2database.com/html/main.html) - H2 es un sistema administrador de bases de datos relacionales programado en Java. Puede ser incorporado en aplicaciones Java o ejecutarse de modo cliente-servidor.
- [CheckStyle](https://checkstyle.sourceforge.io/) - Plugin de maven para codereview. (utilizada en pipeline)
- [PMD](https://pmd.github.io/) - Plugin de maven para codereview. (utilizada en pipeline)

# Ejecución de endpoints
Ejecutar aplicación.

```shell
mvn spring-boot:run
```
Publicar aplicación en Tomcat.

```shell
mvn clean package -DskipTests=true -Dtomcat.url=http://${host}:${port} -Dtomcat.username=${user} -Dtomcat.password=${password} tomcat7:deploy
```
Despliegue en Git CI `(pendiente configurar runner)`
```shell
Pipeline a la rama de produccion
```


Los endpoint estaran alojados en **localhost** en el puerto **8080**

http://localhost:8080/

Url **Swagger**

http://localhost:8080/swagger-ui/index.html

### GET /authenticate
Obtiene JWT con validez de 5 minutos, credenciales por defecto:

username: **admin**

password: **NisumTest**

**Parameters**

|            Nombre | Requerido |  Tipo  | Descripción                                                                                                                                                                                       |
|------------------:|:---------:|:------:|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|        `username` | required  | string | email de usuario.                                                                                                                                                                    |
|        `password` | required  | string | contraseña de usario.                                                                                                                                                                    |

### POST /users/save

Crea un nuevo usuario con telefonos relacionados.

**Parameters**

| Nombre | Requerido |  Tipo   | Descripcion                          |
|-------:|:---------:|:-------:|--------------------------------------|
| `user` | required  | UserDto | Entidad de usuario en formato json. |

**Ejemplo de objeto json**

```
{
    "name": "juan",
    "email": "juan@rodriguez.org",
    "password": "Hunter24",
    "phones": [
        {
            "number": "1234567",
            "citycode": "1",
            "contrycode": "57"
        }
    ]
}

```
### PUT /users/disable/{id}
Desactiva registro de usuario.

**Parameters**

| Nombre | Requerido | Tipo | Descripcion                    |
|-------:|:---------:|:----:|--------------------------------|
|   `id` | required  | UUID | identificador unico de usuario |

**Requiere header Authorization Bearer token**

### PUT /users/active/{id}
Activa registro de usuario.

**Parameters**

| Nombre | Requerido | Tipo | Descripcion                    |
|-------:|:---------:|:----:|--------------------------------|
|   `id` | required  | UUID | identificador unico de usuario |

**Requiere header Authorization Bearer token**

### PUT /users/update/{id}

**Parameters**

| Nombre | Requerido |  Tipo   | Descripcion                         |
|-------:|:---------:|:-------:|-------------------------------------|
| `user` | required  | UserDto | Entidad de usuario en formato json. |
|   `id` | required  |  UUID   | identificador unico de usuario.     |

**Requiere header Authorization Bearer token**

**Objeto json**
```
{
    "name": "pedro Rodriguez",
    "email": "juan@rodriguez.org",
    "password": "Hunter24",
    "phones": [
        {
            "number": "1234567",
            "citycode": "1",
            "contrycode": "57"
        }
    ]
}

```
### GET /users/

**Parameters**

| Nombre | Requerido |  Tipo   | Descripcion                         |
|-------:|:---------:|:-------:|-------------------------------------|
| `-`    | -         | -       | No requiere parametros.             |


**Requiere header Authorization Bearer token**

#DIAGRAMA

**Consulta Endpoint**

![alt text](gitlab/imagenes/diagramaSimple.png)

**Flujo JWT**

![alt text](gitlab/imagenes/62-3-min.png)

**Lista de endpoint**

![alt text](gitlab/imagenes/EndpointSwagger.PNG)

**Inicia sesión** (la validez del token es de 5 minutos)

![alt text](gitlab/imagenes/authenticate-endpoint.PNG)

**Obtiene lista de usuarios** (la validez del token son 5 minutos)

![alt text](gitlab/imagenes/endpoint%20todos%20los%20usuarios.PNG)


## License
  
[Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
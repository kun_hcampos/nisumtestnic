package ni.com.hcampos.NisumTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import ni.com.hcampos.NisumTest.dto.PhoneDto;
import ni.com.hcampos.NisumTest.dto.UserDto;
import ni.com.hcampos.NisumTest.model.PhoneEntity;
import ni.com.hcampos.NisumTest.model.UserEntity;
import ni.com.hcampos.NisumTest.model.jwt.JwtResponse;
import ni.com.hcampos.NisumTest.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.InstanceOfAssertFactories.MAP;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@SpringBootTest
@AutoConfigureMockMvc
class NisumTestApplicationTests {

	private static final ObjectMapper om = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	UserRepository userRepository;

	@Test
	@WithMockUser(username = "admin", password = "NisumTest")
	public void testAuthenticate() throws Exception {
		Map<String, Object> hash = new HashMap<>();
		hash.put("username", "admin");
		hash.put("password", "NisumTest");
		Map<String, Object> token = om.readValue(mockMvc.perform(post("/authenticate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(om.writeValueAsString(hash)))
 				.andDo(print())
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), HashMap.class);
		assertThat(token.get("jwttoken")).isNotEqualTo(null);
	}

	@Test
	@WithMockUser(username = "admin", password = "NisumTest")
	public void testSaveUserEntity() throws Exception {
		UserDto userDto = new UserDto();
		userDto.setName("Nisum Test");
		userDto.setEmail("prueba@nisum.com");
		userDto.setPassword("NisumTest88!!");

		PhoneDto phone = new PhoneDto();
		phone.setNumber("88884444");
		phone.setCityCode("MGA");
		phone.setCountryCode("NIC");

		userDto.setPhones(new HashSet<PhoneDto>() {{
			add(phone);
		}});

		assertThat(om.readValue(mockMvc.perform(post("/users/save")
				.contentType(MediaType.APPLICATION_JSON)
				.content(om.writeValueAsString(userDto)))
				.andDo(print())
				.andExpect(status().isCreated())
				.andReturn().getResponse().getContentAsString(), Object.class)).isNotEqualTo(null);
	}

	@Test
	@WithMockUser(username = "admin", password = "NisumTest")
	public void testDisabled() throws Exception {
		Map<String, Object> hash = new HashMap<>();
		hash.put("username", "admin");
		hash.put("password", "NisumTest");
		Map<String, Object> token = om.readValue(mockMvc.perform(post("/authenticate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(om.writeValueAsString(hash)))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), HashMap.class);

		Map<String, Object> user = om.readValue(mockMvc.perform(
				put("/users/disable/1ac3b15c-3cca-425e-9c4a-22b869aea9c1")
				.header("Authorization", "Bearer ".concat(token.get("jwttoken").toString()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(om.writeValueAsString(new HashMap<>())))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), HashMap.class);

		assertThat(((HashMap<String, Object>)user.get("body")).get("active")).isEqualTo(false);
	}

}

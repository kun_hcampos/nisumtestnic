package ni.com.hcampos.NisumTest.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see MappedSuperclass
 * @since 1.0, <i> Clase que establece atributos de auditoria.</i>
 */
@Getter
@Setter
@MappedSuperclass
public class AuditFields {

    /**
     * contructor por defecto.
     */
    public AuditFields() {

    }

    /**
     * contructor parametrizado.
     * @param created fecha de creacion.
     * @param modified fecha modificacion.
     */
    public AuditFields(final LocalDateTime created, final LocalDateTime modified) {
        this.created = created;
        this.modified = modified;
    }

    /**
     * fecha de creacion.
     */
    @CreatedDate
    @Column(name = "created")
    private LocalDateTime created;

    /**
     * fecha modificacion.
     */
    @LastModifiedDate
    @Column(name = "modified")
    private LocalDateTime modified;

    /**
     * Sobre escribe metodo tostring.
     * @return cadena de caracteres de objeto.
     */
    @Override
    public String toString() {
        return "AuditFields{" + "created=" + created + ", modified=" + modified + '}';
    }
}

package ni.com.hcampos.NisumTest.model.jwt;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;


/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see AllArgsConstructor
 * @since 1.0, <i>Esta clase es necesaria para crear una respuesta que contenga el JWT que se devolverá al usuario.</i>
 */
@Getter
@AllArgsConstructor
public class JwtResponse implements Serializable {

    /**
     * variable de serializacion.
     */
    private static final long serialVersionUID = -8091879091924046844L;

    /**
     * json web token.
     */
    private final String jwttoken;

}

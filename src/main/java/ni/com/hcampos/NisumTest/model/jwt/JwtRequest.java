package ni.com.hcampos.NisumTest.model.jwt;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see AllArgsConstructor
 * @since 1.0, <i> Clase necesaria para almacenar el nombre de usuario y la contraseña que recibimos del cliente.</i>
 */
@Getter
@Setter
@AllArgsConstructor
public class JwtRequest implements Serializable {

    /**
     * variable de serializacion.
     */
    private static final long serialVersionUID = 5926468583005150707L;

    /**
     * usuario de solicitud.
     */
    @ApiModelProperty(value = "Nombre de usuario.",
            name = "username",
            dataType = "String",
            required = true,
            position = 1
    )
    @NotNull(message = "Username: * Requerido.")
    private String username;

    /**
     * contraseña de solicitud.
     */
    @ApiModelProperty(value = "Contraseña.",
            name = "password",
            dataType = "String",
            required = true,
            position = 2
    )
    @NotNull(message = "Contraseña: * Requerido.")
    private String password;

    /**
     * constructor por defecto.
     * necesario para el parseo de json.
     */
    public JwtRequest() {

    }
}

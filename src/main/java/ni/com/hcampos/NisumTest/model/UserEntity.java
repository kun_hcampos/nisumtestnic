package ni.com.hcampos.NisumTest.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see EqualsAndHashCode
 * @see PhoneEntity
 * @see Serializable
 * @since 1.0, <i> Entidad de usuarios.</i>
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "users")
public class UserEntity extends AuditFields implements Serializable {
    /**
     * variable de serializacion.
     */
    private static final long serialVersionUID = 1L;

    /**
     * constructor por defecto.
     */
    public UserEntity() {

    }

    /**
     * Constructor parametrizado.
     * @param id identificador unico de usuario.
     * @param name nombre de usuario.
     * @param email correo de usuario.
     * @param password contraseña del usuario.
     * @param token token del usuario.
     * @param active bandera de usuario activo.
     * @param phones lista de telefonos.
     * @param lastLogin fecha de ultimo inicio de sesion.
     * @param created fecha de creacion.
     * @param modified fecha de modificacion.
     */
    public UserEntity(final UUID id, final String name, final String email, final String password,
                      final String token, final Boolean active, final Set<PhoneEntity> phones,
                      final LocalDateTime lastLogin, final LocalDateTime created, final LocalDateTime modified) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.token = token;
        this.active = active;
        this.phones = phones;
        this.lastLogin = lastLogin;
        this.setCreated(created);
        this.setModified(modified);
    }

    /**
     * identificador unico del usuario.
     */
    @Id
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @GeneratedValue(generator = "uuid")
    @Column(name = "id")
    private UUID id;

    /**
     * nombre de usuario.
     */
    @NotEmpty(message = "Nombre: * Requerido.")
    @Column(name = "name")
    private String name;

    /**
     * correo del usuario.
     */
    @Email(regexp = "^[^@]+@[^@]+\\.[a-zA-Z]{2,}$", message = "Email: * No válido.")
    @NotNull(message = "Email: * Requerido.")
    @Column(name = "email")
    private String email;

    /**
     * contraseña del usuario.
     */
    @NotEmpty(message = "Contraseña: * Requerida.")
    @Column(name = "password")
    private String password;

    /**
     * token del usuario.
     */
    @Column(name = "token")
    private String token;

    /**
     * bandera de activo.
     */
    @Column(name = "is_active")
    private Boolean active;

    /**
     * lista de telefonos.
     */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<PhoneEntity> phones = new HashSet<>();

    /**
     * ultimo login.
     */
    @Column(name = "last_login")
    private LocalDateTime lastLogin;

}

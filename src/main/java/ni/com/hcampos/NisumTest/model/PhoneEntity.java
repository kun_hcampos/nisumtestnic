package ni.com.hcampos.NisumTest.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see EqualsAndHashCode
 * @see ToString
 * @see Entity
 * @see Table
 * @see UserEntity
 * @see Serializable
 * @since 1.0, <i> Entidad de Telefonos.</i>
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "phones")
public class PhoneEntity extends AuditFields  implements Serializable {
    /**
     * variable de serializacion.
     */
    private static final long serialVersionUID = 1L;

    /**
     * contructor por defecto.
     */
    public PhoneEntity() {
    }

    /**
     * Contructor parametrizado.
     * @param id identificador unico.
     * @param user propietario de telefono.
     * @param number numero de telefono.
     * @param cityCode codigo de ciudad.
     * @param countryCode codigo de pais.
     */
    public PhoneEntity(final Integer id, final UserEntity user, final String number, final String cityCode,
                       final String countryCode) {
        this.id = id;
        this.user = user;
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
    }

    /**
     * Contructor parametrizado.
     * @param id identificador unico.
     * @param user propietario de telefono.
     * @param number numero de telefono.
     * @param cityCode codigo de ciudad.
     * @param countryCode codigo de pais.
     * @param created fecha de creacion.
     * @param modified fecha modificacion.
     */
    public PhoneEntity(final Integer id, final UserEntity user, final String number, final String cityCode,
                       final String countryCode, final LocalDateTime created, final LocalDateTime modified) {
        this.id = id;
        this.user = user;
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
        this.setCreated(created);
        this.setModified(modified);
    }

    /**
     * identificador unico.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * usuario propietario de telefono.
     */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private UserEntity user;

    /**
     * numero de telefono.
     */
    @Column(name = "number")
    private String number;

    /**
     * codigo de ciudad.
     */
    @Column(name = "city_code")
    private String cityCode;

    /**
     * codigo de pais.
     */
    @Column(name = "country_code")
    private String countryCode;

}

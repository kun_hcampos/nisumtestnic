package ni.com.hcampos.NisumTest.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @since 1.0, <i>JwtTokenUtil es responsable de realizar operaciones JWT como la creación y validación.
 * Hace uso de io.jsonwebtoken.Jwts para lograr esto.</i>
 */
@Component
public class JwtTokenUtil implements Serializable {

    /**
     * variable de serializacion.
     */
    private static final long serialVersionUID = -2550185165626007488L;

    /**
     * valor de expiracion de jwt.
     */
    public static final long JWT_TOKEN_VALIDITY = 5 * 60; // 5 minutos

    /**
     * secreto del jwt.
     */
    @Value("${jwt.secret}")
    private String secret;

    //retrieve username from jwt token

    /**
     * Recupera nombre apartir del token.
     * @param token jwt.
     * @return nombre del usuario.
     */
    public String getUsernameFromToken(final String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    /**
     * Recuperar la fecha de caducidad del token jwt.
     * @param token jwt.
     * @return fecha de expiracion.
     */
    public Date getExpirationDateFromToken(final String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     * Recupera Claims generico.
     * @param token jwt.
     * @param claimsResolver Resolvedor de reclamacion.
     * @param <T> generico.
     * @return Claims Generico.
     */
    public <T> T getClaimFromToken(final String token, final Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /**
     * Recupera cualquier información del token, necesitaremos la clave secreta.
     * @param token jwt.
     * @return Claims jwt.
     */
    private Claims getAllClaimsFromToken(final String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    /**
     * Varifica si el token expiro.
     * @param token token jwt.
     * @return valor booleano indicado validez del token.
     */
    private Boolean isTokenExpired(final String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     * Generador de jwt, apartir de la informacion de usuario.
     * @param userDetails informacion de usuario.
     * @return token jwt.
     */
    public String generateToken(final UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername());
    }

    /**
     * Generador de jwt, apartir del nombre de usuario.
     * @param username nombre de usuario.
     * @return token jwt.
     */
    public String generateToken(final String username) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, username);
    }

    /**
     * Mientras crea el token,
     * <ol>
     *     <li>Define claims del token, como Issuer, Expiration, Subject, y el ID</li>
     *     <li>Firma el JWT utilizando el algoritmo HS512 y la clave secreta</li>
     *     <li>De acuerdo con JWS Compact</li>
     * </ol>
     * Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
     * Compactación de la JWT a una cadena segura para URL(feminine).
     * @param claims instancia de claims.
     * @param subject cadena de caracteres.
     * @return jwt.
     */
    private String doGenerateToken(final Map<String, Object> claims, final String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    /**
     * Valida Token.
     * @param token token,
     * @param userDetails instancia de user details
     * @return valor booleano indicado validez del token.
     */
    public Boolean validateToken(final String token, final UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
    }

}

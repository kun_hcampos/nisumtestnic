package ni.com.hcampos.NisumTest.service;

import ni.com.hcampos.NisumTest.dto.UserDto;
import ni.com.hcampos.NisumTest.exception.NisumException;
import ni.com.hcampos.NisumTest.model.UserEntity;

import java.util.List;
import java.util.UUID;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @since 1.0, <i>Interface de servicio para establecer comportamientos de Users.</i>
 * @see UserEntity
 * @see NisumException
 */
public interface IUserService {

    /**
     * Obtiene lista de usuarios.
     * @return Lista de usuarios.
     */
    List<UserEntity> findAll();

    /**
     * Valida y guarda registros de usuarios.
     * @param user usuario dto para validaciones.
     * @return user entity.
     * @throws NisumException checked exception.
     */
    UserEntity save(UserDto user) throws NisumException;

    /**
     * Valida y actualiza registros de usuarios.
     * @param userDto usuario dto para validaciones.
     * @param id identificador unico de usuario.
     * @return user entity.
     * @throws NisumException checked exception.
     */
    UserEntity update(UserDto userDto, UUID id) throws NisumException;

    /**
     * Desactiva usuario.
     * @param id identificador unico.
     * @return entidad de usuario.
     * @throws NisumException checked exception.
     */
    UserEntity disabled(UUID id) throws NisumException;

    /**
     * Activa usuario.
     * @param id identificador unico.
     * @return entidad de usuario.
     * @throws NisumException checked exception.
     */
    UserEntity active(UUID id) throws NisumException;
}

package ni.com.hcampos.NisumTest.service;

import ni.com.hcampos.NisumTest.exception.NisumException;
import ni.com.hcampos.NisumTest.model.UserEntity;
import ni.com.hcampos.NisumTest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see UserDetailsService
 * @since 1.0, <i>JWTUserDetailsService implementa la interfaz Spring Security UserDetailsService. Reemplaza el
 * loadUserByUsername para obtener detalles de usuario de la base de datos utilizando el nombre de usuario. Spring
 * Security Authentication Manager llama a este método para obtener los detalles del usuario de la base de datos al
 * autenticar los detalles del usuario proporcionados por el usuario.</i>
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {

    /**
     * referencia a repositorio de usuarios.
     */
    private final UserRepository userRepository;

    /**
     * Contructor parametrizado.
     * @param userRepository inicializa dependencia.
     */
    @Autowired
    public JwtUserDetailsService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * verifica existencia de usuario (Password NisumTest).
     *
     * @param username nombre de usuario.
     * @return objecto user details.
     * @throws UsernameNotFoundException unchecked exception.
     */
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        if ("admin".equals(username)) {
            return new User("admin", "$2a$10$6nIQquStdc8IyKMIZ.rvaeoxrBGpYmQbjvQYYKrNa6tZeh.OiEsgy",
                    new ArrayList<>());
        }
        Optional<UserEntity> userEntity = userRepository.findUserEntityByEmail(username);
        if (userEntity.isPresent()) {
            userEntity.get().setLastLogin(LocalDateTime.now());
            if (!userEntity.get().getActive()) {
                throw new DisabledException("El usuario con el nombre "
                        .concat(username)
                        .concat(" se encuentra inactivo."));
            }
            userRepository.save(userEntity.get());
            return new User(userEntity.get().getEmail(), userEntity.get().getPassword(), new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("No se encontro el usuario con el nombre: ".concat(username));
        }
    }
}

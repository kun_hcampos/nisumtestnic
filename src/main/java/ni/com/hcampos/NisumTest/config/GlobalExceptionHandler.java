package ni.com.hcampos.NisumTest.config;

import ni.com.hcampos.NisumTest.dto.ErrorMessage;
import ni.com.hcampos.NisumTest.exception.NisumException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see ControllerAdvice
 * @see ResponseBody
 * @see ExceptionHandler
 * @see ResponseStatus
 * @see NisumException
 * @see MethodArgumentNotValidException
 * @see Exception
 * @see Serializable
 * @see ErrorMessage
 * @since 1.0, <i> Proporciona la capacidad de interceptar las excepciones generadas.</i>
 */
@ControllerAdvice
public class GlobalExceptionHandler implements Serializable {
    /** Variable de serialización. */
    private static final long serialVersionUID = 1L;

    /**
     * Intercepta todas las excepciones de la clase Exception.
     * @param ex Exception generada.
     * @return ErrorMessage Response generico para errores.
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleBusinessException(final Exception ex) {
        return new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                ex.getMessage(), Objects.isNull(ex.getCause()) ? "" : ex.getCause().getMessage());
    }

    /**
     * Intercepta todas las excepciones de la clase NisumException.
     * @param ex Exception generada.
     * @return ErrorMessage Response generico para errores.
     */
    @ResponseBody
    @ExceptionHandler(NisumException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleBusinessException(final NisumException ex) {
        return new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                ex.getMessage(), Objects.isNull(ex.getCause()) ? "" : ex.getCause().getMessage());
    }

    /**
     * Intercepta todas las excepciones de la clase MethodArgumentNotValidException.
     * @param ex Exception generada.
     * @return ErrorMessage Response generico para errores.
     */
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {
        return new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "El módelo no es válido.", ex.getBindingResult().getAllErrors().stream()
                .map(e -> e.getDefaultMessage()).collect(Collectors.joining(", ")));
    }

    /**
     * Intercepta todas las excepciones de la clase UsernameNotFoundException.
     * @param ex Exception generada.
     * @return ErrorMessage Response generico para errores.
     */
    @ResponseBody
    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleUsernameNotFoundException(final UsernameNotFoundException ex) {
        return new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(),
                Objects.isNull(ex.getCause()) ? "" : ex.getCause().getMessage());
    }
}

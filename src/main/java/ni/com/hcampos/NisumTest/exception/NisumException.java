package ni.com.hcampos.NisumTest.exception;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Exception
 * @since 1.0, <i> Exception customizada usada para validaciones de logica de negocio.</i>
 */
public class NisumException extends  Exception {

    /**
     * constructor por defecto.
     */
    public NisumException() {
    }

    /**
     * contructor  parametrizado.
     * @param message mensaje de la exception.
     */
    public NisumException(final String message) {
        super(message);
    }

    /**
     * contructor  parametrizado.
     * @param message mensaje de la exception.
     * @param cause causa de la exception.
     */
    public NisumException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * contructor  parametrizado.
     * @param cause causa de la exception.
     */
    public NisumException(final Throwable cause) {
        super(cause);
    }

    /**
     * contructor  parametrizado.
     * @param message mensaje de la exception.
     * @param cause causa de la exception.
     * @param enableSuppression bandera de suppresion.
     * @param writableStackTrace bandera writable stack trace.
     */
    public NisumException(final String message, final Throwable cause, final boolean enableSuppression,
                          final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

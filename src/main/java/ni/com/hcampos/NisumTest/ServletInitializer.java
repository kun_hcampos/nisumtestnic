package ni.com.hcampos.NisumTest;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see SpringBootServletInitializer
 */
public class ServletInitializer extends SpringBootServletInitializer {
    /**
     * Metodo de contrusccion de SpringApplicationBuilder.
     * @param application instancia de aplicacion.
     * @return SpringApplicationBuilder.
     */
    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
        return application.sources(NisumTestApplication.class);
    }
}

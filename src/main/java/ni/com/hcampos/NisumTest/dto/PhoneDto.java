package ni.com.hcampos.NisumTest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import ni.com.hcampos.NisumTest.model.PhoneEntity;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see UserDto
 * @see Serializable
 * @since 1.0, <i> Proporciona la estructura para implementacion de patron dto para la clase de telefonos,
 * se establecen validaciones.</i>
 */
@Getter
@Setter
public class PhoneDto implements Serializable {

    /**
     * variable de serializacion.
     */
    private static final long serialVersionUID = 1L;

    /**
     * contructor por defecto.
     */
    public PhoneDto() {
    }

    /**
     * Contructor parametrizado.
     * @param phoneEntity objeto entidad inicializa atributos locales.
     */
    public PhoneDto(final PhoneEntity phoneEntity) {
        this.id = phoneEntity.getId();
        this.number = phoneEntity.getNumber();
        this.cityCode = phoneEntity.getCityCode();
        this.countryCode = phoneEntity.getCountryCode();
    }

    /**
     * Contructor parametrizado.
     * @param id identificador unico.
     * @param number numero de telefono.
     * @param cityCode codigo de ciudad.
     * @param countryCode codigo de pais.
     */
    public PhoneDto(final Integer id, final String number, final String cityCode, final String countryCode) {
        this.id = id;
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
    }

    /**
     * identificador unico.
     */
    @ApiModelProperty(value = "Identificador unico.",
            name = "id",
            dataType = "UUID",
            required = false,
            position = 1
    )
    private Integer id;

    /**
     * numero de telefono.
     */
    @ApiModelProperty(value = "Número de telefono.",
            name = "number",
            dataType = "String",
            required = true,
            position = 2
    )
    @NotEmpty(message = "Teléfono número: * Requerido.")
    @JsonProperty("number")
    private String number;

    /**
     * codigo de ciudad.
     */
    @ApiModelProperty(value = "Codigo de ciudad",
            name = "cityCode",
            dataType = "String",
            required = true,
            position = 3
    )
    @NotEmpty(message = "Teléfono código de ciudad: * Requerido.")
    @JsonProperty("citycode")
    private String cityCode;

    /**
     * codigo de pais.
     */
    @ApiModelProperty(value = "Codigo de pais",
            name = "countryCode",
            dataType = "String",
            required = true,
            position = 4
    )
    @NotEmpty(message = "Teléfono código de pais: * Requerido.")
    @JsonProperty("contrycode")
    private String countryCode;

}

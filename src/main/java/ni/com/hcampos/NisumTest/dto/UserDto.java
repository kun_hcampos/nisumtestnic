package ni.com.hcampos.NisumTest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ni.com.hcampos.NisumTest.model.PhoneEntity;
import ni.com.hcampos.NisumTest.model.UserEntity;

import javax.persistence.Column;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see AllArgsConstructor
 * @see JsonInclude
 * @see PhoneDto
 * @see Serializable
 * @since 1.0, <i> Proporciona la estructura para implementacion de patron dto para la clase de usuarios,
 * se establecen validaciones.</i>
 */
@Getter
@Setter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto implements Serializable {

    /**
     * variable de serializacion.
     */
    private static final long serialVersionUID = 1L;

    /**
     * constructor por defecto.
     */
    public UserDto() {

    }

    /**
     * constructor parametrizado.
     * @param userEntity objecto de tipo entidad, inicializa propiedades locales.
     */
    public UserDto(final UserEntity userEntity) {
        this.id = userEntity.getId();
        this.name = userEntity.getName();
        this.email = userEntity.getEmail();
        this.password = userEntity.getPassword();
        this.token = userEntity.getToken();
        this.active = userEntity.getActive();
        this.phones = userEntity.getPhones().stream().map(p -> new PhoneDto(p)).collect(Collectors.toSet());
        this.created = userEntity.getCreated();
        this.modified = userEntity.getModified();
        this.lastLogin = userEntity.getLastLogin();
    }

    /**
     * Constructor parametrizado.
     * @param id identificador unico de usuario.
     * @param name nombre de usuario.
     * @param email correo de usuario.
     * @param password contraseña del usuario.
     * @param token token del usuario.
     * @param active bandera de usuario activo.
     * @param phones lista de telefonos.
     */
    public UserDto(final UUID id, final String name, final String email, final String password, final String token,
                   final Boolean active, final Set<PhoneDto> phones) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.token = token;
        this.active = active;
        this.phones = phones;
    }

    /***
     * identificador unico del usuario.
     */
    @ApiModelProperty(value = "Identificador unico.",
            name = "id",
            dataType = "Integer",
            required = false,
            position = 1
    )
    @JsonProperty("id")
    private UUID id;

    /**
     * nombre de usuario.
     */
    @ApiModelProperty(value = "Nombre de usuario.",
            name = "id",
            dataType = "name",
            required = true,
            position = 2
    )
    @NotEmpty(message = "Nombre: * Requerido.")
    @JsonProperty("name")
    private String name;

    /***
     * correo del usuario.
     */
    @ApiModelProperty(value = "Correo electronico.",
            name = "email",
            dataType = "String",
            required = true,
            position = 3
    )
    @NotNull(message = "Email: * Requerido.")
    @Email(regexp = "^[^@]+@[^@]+\\.[a-zA-Z]{2,}$", message = "Email: * No válido.")
    @JsonProperty("email")
    private String email;

    /**
     * contraseña del usuario.
     */
    @ApiModelProperty(value = "Contraseña.",
            name = "pasword",
            dataType = "String",
            required = true,
            position = 4
    )
    @NotEmpty(message = "Contraseña: * Requerida.")
    @Column(name = "password")
    @JsonProperty("password")
    private String password;

    /**
     * token del usuario.
     */
    @ApiModelProperty(value = "Token.",
            name = "token",
            dataType = "String",
            required = false,
            position = 5
    )
    @JsonProperty("token")
    private String token;

    /**
     * bandera de activo.
     */
    @ApiModelProperty(value = "Estado.",
            name = "is_active",
            dataType = "Boolean",
            required = false,
            position = 6
    )
    @JsonProperty("active")
    private Boolean active;

    /**
     * lista de telefonos.
     */
    @ApiModelProperty(value = "Telefonos",
            name = "phones",
            dataType = "List",
            required = true,
            position = 7
    )
    @NotNull(message = "Telefonos: * Debe proporcionar almenos un número de contacto.")
    @Size(min = 1, message = "Telefonos: * Debe proporcionar almenos un número de contacto.")
    @JsonProperty("phones")
    private @Valid Set<PhoneDto> phones;

    /**
     * ultimo login.
     */
    @JsonProperty("last_login")
    private LocalDateTime lastLogin;

    /**
     * fecha de creacion.
     */
    @JsonProperty("created")
    private LocalDateTime created;

    /**
     * fecha de modificacion.
     */
    @JsonProperty("modified")
    private LocalDateTime modified;

    /**
     * Metodo utilitario para convertir dto a entidad.
     * @return entidad de tipo UserEntity
     * @see UserEntity
     */
    public UserEntity toUserEntity() {
        return new UserEntity(this.id, this.name, this.email, this.password, this.token, this.active,
                phones.stream().map(p ->
                        new PhoneEntity(p.getId(), null, p.getNumber(), p.getCityCode(), p.getCountryCode()))
                        .collect(Collectors.toSet()), null, LocalDateTime.now(), null);
    }

    /**
     * Metodo utilitario para obtener response en formato solicitado, elimina datos de confidencialidad.
     * @return instancia dto con eliminacion de algunos datos.
     */
    public UserDto createApiResponse() {
        this.email = null;
        this.phones = null;
        this.name = null;
        this.password = null;
        return this;
    }

}

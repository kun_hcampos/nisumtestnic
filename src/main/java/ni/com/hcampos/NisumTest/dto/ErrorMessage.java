package ni.com.hcampos.NisumTest.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see AllArgsConstructor
 * @since 1.0, <i> Proporciona estructura generica para el cuerpo del error de APIs.</i>
 */
@Getter
@Setter
@AllArgsConstructor
public class ErrorMessage {

    /**
     * Variable para visualizar el status code de la peticion.
     */
    private Integer statusCode;
    /**
     * Mensaje personalizado del response.
     */
    private String message;
    /**
     * Mensaje detallado del error.
     */
    private String detail;

}

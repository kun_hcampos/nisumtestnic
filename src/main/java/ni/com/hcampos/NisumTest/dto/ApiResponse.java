package ni.com.hcampos.NisumTest.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see AllArgsConstructor
 * @since 1.0, <i> Proporciona estructura generica para el cuerpo del resultados de las APIs.</i>
 * @param <T> Generico de la clase.
 */
@Getter
@Setter
@AllArgsConstructor
public class ApiResponse<T> {

    /**
     * Tipo generica para establecer distintos tipos de resultados.
     */
    private T body;
    /**
     * Variable para visualizar el status code de la peticion.
     */
    private Integer statusCode;

    /**
     * Mensaje personalizado del response.
     */
    private String message;

    /**
     * constructor por defecto.
     */
    public ApiResponse() {

    }

    /**
     * Contructor parametrizado para establecer el cuerpo de la instancia.
     * @param body establece el cuerpo del response al instanciar la clase.
     */
    public ApiResponse(final T body) {
        this.body = body;
    }

}

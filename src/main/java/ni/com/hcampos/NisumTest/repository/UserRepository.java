package ni.com.hcampos.NisumTest.repository;

import ni.com.hcampos.NisumTest.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Repository
 * @see UserEntity
 * @see UUID
 * @since 1.0, <i> Clase repository para Phone.</i>
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    /**
     * Obtiene UsuarioEntity atraves de filtro.
     * @param email filtro de correo electronico.
     * @return Optional de tipo UserEntity.
     */
    Optional<UserEntity> findUserEntityByEmail(String email);

}

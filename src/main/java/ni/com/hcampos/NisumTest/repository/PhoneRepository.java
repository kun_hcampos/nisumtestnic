package ni.com.hcampos.NisumTest.repository;

import ni.com.hcampos.NisumTest.model.PhoneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see Repository
 * @see PhoneEntity
 * @since 1.0, <i> Clase repository para Phone.</i>
 */
@Repository
public interface PhoneRepository extends JpaRepository<PhoneEntity, Integer> {
}

package ni.com.hcampos.NisumTest.servicesImpl;

import ni.com.hcampos.NisumTest.service.IPhoneService;
import org.springframework.stereotype.Service;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @since 1.0, <i>Interface de servicio para establecer comportamientos de Phones.</i>
 * @see IPhoneService
 */
@Service
public class PhoneServiceImpl implements IPhoneService {
}

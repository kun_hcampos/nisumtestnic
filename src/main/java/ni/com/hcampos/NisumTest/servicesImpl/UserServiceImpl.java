package ni.com.hcampos.NisumTest.servicesImpl;

import ni.com.hcampos.NisumTest.dto.UserDto;
import ni.com.hcampos.NisumTest.exception.NisumException;
import ni.com.hcampos.NisumTest.model.UserEntity;
import ni.com.hcampos.NisumTest.repository.UserRepository;
import ni.com.hcampos.NisumTest.service.IUserService;
import ni.com.hcampos.NisumTest.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @since 1.0, <i>Implementacion de interface de servicio para establecer comportamientos de Users.</i>
 * @see UserEntity
 * @see NisumException
 * @see Service
 */
@Service
public class UserServiceImpl implements IUserService {

    /**
     * Repositorio de Usuarios.
     */
    private final UserRepository userRepository;

    /**
     * Bean password encoder.
     */
    private final PasswordEncoder passwordEncoder;

    /**
     * Bean JWT Token util.
     */
    private final JwtTokenUtil jwtTokenUtil;

    /**
     * variable con expresion regular, usada para validar la complejidad de la contraseña.
     */
    @Value("${pasword.regexp}")
    private String paswordRegexp;

    /**
     * Contructor parametrizado.
     * @param userRepository dependencia de bean.
     * @param jwtTokenUtil dependencia de bean.
     * @see UserRepository
     * @see JwtTokenUtil
     */
    @Autowired
    public UserServiceImpl(final UserRepository userRepository, final JwtTokenUtil jwtTokenUtil) {
        this.userRepository = userRepository;
        this.passwordEncoder = new BCryptPasswordEncoder();
        this.jwtTokenUtil = jwtTokenUtil;
    }

    /**
     * Obtiene lista de usuarios.
     * @return Lista de usuarios.
     */
    @Override
    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }

    /**
     * Valida y guarda registros de usuarios.
     * @param user usuario dto para validaciones.
     * @return user entity.
     * @throws NisumException checked exception.
     */
    @Override
    public UserEntity save(final UserDto user) throws NisumException {
        validPassword(user.getPassword());
        validEmail(user.getEmail());
        UserEntity entity = user.toUserEntity();
        entity.setPassword(passwordEncoder.encode(user.getPassword()));
        entity.setCreated(LocalDateTime.now());
        entity.setToken(jwtTokenUtil.generateToken(user.getEmail()));
        entity.setLastLogin(entity.getCreated());
        entity.setActive(true);
        entity.getPhones().forEach(p -> {
            p.setUser(entity);
            p.setCreated(LocalDateTime.now());
        });
        return userRepository.save(entity);
    }

    /**
     * Valida y actualiza registros de usuarios.
     * @param userDto usuario dto para validaciones.
     * @param id identificador unico de usuario.
     * @return user entity.
     * @throws NisumException checked exception.
     */
    @Override
    public UserEntity update(final UserDto userDto, final UUID id) throws NisumException {
        Optional<UserEntity> entity = userRepository.findById(id);
        if (!entity.isPresent()) {
            throw new NisumException("No se encontro al usuario con identificador: ".concat(id.toString()));
        } else {
            UserEntity user = entity.get();
            user.setName(userDto.getName());
            user.setModified(LocalDateTime.now());
            userRepository.save(user);
            return user;
        }
    }

    /**
     * Desactiva usuario.
     * @param id identificador unico.
     * @return entidad de usuario.
     * @throws NisumException checked exception.
     */
    @Override
    public UserEntity disabled(final UUID id) throws NisumException {
        Optional<UserEntity> entity = userRepository.findById(id);
        if (!entity.isPresent()) {
            throw new NisumException("No se encontro al usuario con identificador: ".concat(id.toString()));
        } else {
            UserEntity user = entity.get();
            if (!user.getActive()) {
                throw new NisumException("El usuario con identificador: ".concat(id.toString())
                        .concat(", ya se encontraba bloqueado."));
            }
            user.setActive(false);
            user.setModified(LocalDateTime.now());
            userRepository.save(user);
            return user;
        }
    }

    /**
     * Activa usuario.
     * @param id identificador unico.
     * @return entidad de usuario.
     * @throws NisumException checked exception.
     */
    @Override
    public UserEntity active(final UUID id) throws NisumException {
        Optional<UserEntity> entity = userRepository.findById(id);
        if (!entity.isPresent()) {
            throw new NisumException("No se encontro al usuario con identificador: ".concat(id.toString()));
        } else {
            UserEntity user = entity.get();
            if (user.getActive()) {
                throw new NisumException("El usuario con identificador: ".concat(id.toString())
                        .concat(", ya se encontraba activo."));
            }
            user.setActive(true);
            user.setModified(LocalDateTime.now());
            userRepository.save(user);
            return user;
        }
    }

    /**
     * Válida que la contraseña cumpla con la expresion regular.
     * @param password contraseña a evaluar.
     * @throws NisumException checked exception.
     */
    private void validPassword(final String password) throws NisumException {
        Pattern pattern = Pattern.compile(paswordRegexp);
        Matcher matcher = pattern.matcher(password);
        if (!matcher.matches()) {
            throw new NisumException("Contraseña: * No cumple con los requerimientos minimos.");
        }
    }

    /**
     * Válida que el correo no exista en la base de datos.
     * @param email correo electronico a validar.
     * @throws NisumException checked exception.
     */
    private void validEmail(final String email) throws NisumException {
        Optional<UserEntity> user = userRepository.findUserEntityByEmail(email);
        if (user.isPresent()) {
            throw new NisumException("Email: * No disponible.");
        }
    }

}

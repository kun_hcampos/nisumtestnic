package ni.com.hcampos.NisumTest.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import ni.com.hcampos.NisumTest.dto.ApiResponse;
import ni.com.hcampos.NisumTest.dto.ErrorMessage;
import ni.com.hcampos.NisumTest.dto.UserDto;
import ni.com.hcampos.NisumTest.exception.NisumException;
import ni.com.hcampos.NisumTest.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see RestController
 * @see RequestMapping
 * @see Autowired
 * @see GetMapping
 * @see PostMapping
 * @see PutMapping
 * @since 1.0, <i> Proporciona la configuracion para la implementacion del api de usuarios.</i>
 */
@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    /**
     * Implementacion del servicio de usuario.
     */
    private final IUserService userService;

    /**
     * Constructor de la clase, inicializa dependencias.
     *
     * @param userService inyeccion de dependecia user service.
     */
    @Autowired
    public UserController(final IUserService userService) {
        this.userService = userService;
    }

    /**
     * Crea punto para obtener lista de usuarios del sistema.
     *
     * @return Response con lista de usuario
     * @see ApiResponse
     * @see UserDto
     * @see ni.com.hcampos.NisumTest.dto.PhoneDto
     */
    @ApiOperation(value = "Obtiene lista de usuarios", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Lista de usuarios obtenida con éxito.",
                    response = ApiResponse.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "Ha ocurrido un error inesperado.",
                    response = ApiResponse.class),
    })
    @GetMapping("/")
    public ResponseEntity<ApiResponse<List<UserDto>>> getAll() {
        ApiResponse<List<UserDto>> response = new ApiResponse<>(new ArrayList<>());
        response.setBody(userService.findAll().stream().map(x -> new UserDto(x)).collect(Collectors.toList()));
        response.setMessage("Lista de usuarios obtenida con éxito.");
        response.setStatusCode(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Crea punto para guardar registros de usuarios.
     *
     * @param user obtejeto de tipo user dto para validar y guardar usuario.
     * @return Response con lista de usuario
     * @throws NisumException checked exception
     * @see ApiResponse
     * @see UserDto
     * @see ni.com.hcampos.NisumTest.dto.PhoneDto
     */
    @ApiOperation(value = "Registra un nuevo usuario", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 201, message = "Usuario registrado con éxito.",
                    response = ApiResponse.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "Mensaje de error.",
                    response = ErrorMessage.class),
    })
    @PostMapping("/save")
    public ResponseEntity<ApiResponse<UserDto>> save(@Valid @RequestBody final UserDto user)
            throws NisumException {
        ApiResponse<UserDto> response = new ApiResponse<>();
        UserDto entity = new UserDto(userService.save(user)).createApiResponse();
        response.setBody(entity);
        response.setMessage("Usuario registrado con éxito.");
        response.setStatusCode(HttpStatus.CREATED.value());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /***
     * crea punto para actualizar registros de usuarios.
     * @param user objecto de tipo user dto para validar y actualizar registros de usuario.
     * @param id identificador unico de usuario.
     * @return Response con lista de usuario.
     * @throws NisumException checked exception
     * @see ApiResponse
     * @see UserDto
     * @see ni.com.hcampos.NisumTest.dto.PhoneDto
     */
    @ApiOperation(value = "Actualiza el nombre de un usuario existente.", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Usuario actualizado con éxito.",
                    response = ApiResponse.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "Mensaje de error.",
                    response = ErrorMessage.class),
    })
    @PutMapping(value = "/update/{id}")
    public ResponseEntity<ApiResponse<UserDto>> updateUser(final UserDto user, @PathVariable final UUID id)
            throws NisumException {
        ApiResponse<UserDto> response = new ApiResponse<>(null);
        UserDto entity = new UserDto(userService.update(user, id));
        response.setBody(entity.createApiResponse());
        response.setMessage("Usuario actualizado con éxito.");
        response.setStatusCode(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * crea punto para desactivar usuario.
     * @param id identificador unico de usuario
     * @return Usuario actualizado.
     * @throws NisumException checked exception.
     */
    @ApiOperation(value = "Bloquea usuario.", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Usuario ha sido bloquedo con éxito.",
                    response = ApiResponse.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "Mensaje de error.",
                    response = ErrorMessage.class),
    })
    @PutMapping(value = "/disable/{id}")
    public ResponseEntity<ApiResponse<UserDto>> disable(@PathVariable final UUID id) throws NisumException {
        ApiResponse<UserDto> response = new ApiResponse<>(new UserDto(userService.disabled(id)).createApiResponse());
        response.setMessage("El usuario con identificador ".concat(id.toString())
                .concat(", ha sido bloquedo con éxito"));
        response.setStatusCode(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * crea punto para activar usuario.
     * @param id identificador unico de usuario
     * @return Usuario actualizado.
     * @throws NisumException checked exception.
     */
    @ApiOperation(value = "Activa usuario.", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Usuario ha sido activado con éxito.",
                    response = ApiResponse.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "Mensaje de error.",
                    response = ErrorMessage.class),
    })
    @PutMapping(value = "/active/{id}")
    public ResponseEntity<ApiResponse<UserDto>> active(@PathVariable final UUID id) throws NisumException {
        ApiResponse<UserDto> response = new ApiResponse<>(new UserDto(userService.active(id)).createApiResponse());
        response.setMessage("El usuario con identificador ".concat(id.toString())
                .concat(", ha sido activado con éxito"));
        response.setStatusCode(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}

package ni.com.hcampos.NisumTest.controller.jwt;


import ni.com.hcampos.NisumTest.exception.NisumException;
import ni.com.hcampos.NisumTest.model.jwt.JwtRequest;
import ni.com.hcampos.NisumTest.model.jwt.JwtResponse;
import ni.com.hcampos.NisumTest.service.JwtUserDetailsService;
import ni.com.hcampos.NisumTest.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see RestController
 * @see CrossOrigin
 * @see Autowired
 * @see PostMapping
 * @since 1.0, <i> Proporciona la configuracion para la implementacion del api de authenticacion jwt.</i>
 */
@RestController
@CrossOrigin
public class JwtAuthenticationController {

    /**
     * Definicion de bean AuthenticationManager.
     */
    private final AuthenticationManager authenticationManager;

    /**
     * Definicion de bean JwtTokenUtil.
     */
    private final JwtTokenUtil jwtTokenUtil;

    /**
     * Definicion de Services JwtUserDetailsService.
     */
    private final JwtUserDetailsService userDetailsService;

    /**
     * Contructor de la clase inicializa dependencias.
     * @param authenticationManager inyeccion de dependencia.
     * @param jwtTokenUtil inyeccion de dependencia.
     * @param userDetailsService inyeccion de dependencia.
     */
    @Autowired
    public JwtAuthenticationController(final AuthenticationManager authenticationManager,
                                       final JwtTokenUtil jwtTokenUtil,
                                       final JwtUserDetailsService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }

    /**
     * Establece un punto de validacion para autenticacion.
     * @param authenticationRequest Solicitud de autenticacion.
     * @return JwtResponse token.
     * @throws Exception checked exception.
     * @See JwtResponse
     */
    @PostMapping(value = "/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody final JwtRequest authenticationRequest)
            throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    /**
     * Valida usuario y contraseña atraves del authentication manager.
     * @param username nombre del usuario.
     * @param password contraseña del usuario.
     * @throws Exception checked exception.
     */
    private void authenticate(final String username, final String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new NisumException("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new NisumException("INVALID_CREDENTIALS", e);
        }
    }
}

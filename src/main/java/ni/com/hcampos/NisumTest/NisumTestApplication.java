package ni.com.hcampos.NisumTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022.
 * @version 1.0
 * @see EnableSwagger2
 * @see SpringBootApplication
 * @since 1.0, <i> Clase de inicializacion del proyecto.</i>
 */
@EnableSwagger2
@SpringBootApplication
public class NisumTestApplication {

    /**
     * punto de entrada de jvm.
     * @param args arreglo de parametros.
     */
    public static void main(final String[] args) {
        SpringApplication.run(NisumTestApplication.class, args);
    }

}

-- Insertando usuario
INSERT INTO users (id, name, email, password, last_login, token, is_active,created, modified) VALUES
('1ac3b15c-3cca-425e-9c4a-22b869aea9c1',  'Hilario Antonio Campos', 'hilarioantoniocs@gmail.com', '$2a$10$6nIQquStdc8IyKMIZ.rvaeoxrBGpYmQbjvQYYKrNa6tZeh.OiEsgy', '2021-09-24T15:28:03.123456789', 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJsdWNob0BnbWFpbC5jb20iLCJyb2xlcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9BRE1JTiJ9XSwiaWF0IjoxNjMyNzE1NjQ0LCJleHAiOjE2MzI3MTYyNDR9.tCqhdF_u9fsr-C9YMgfM8J-bcL-CJTIgDzuvpNI5MII', true,'2022-04-05T01:28:03', null);

-- PHONES
INSERT INTO phones (user_id, number, city_code, country_code,created, modified) VALUES
('1ac3b15c-3cca-425e-9c4a-22b869aea9c1', '87752819', 'MNG', 'NIC','2022-04-09T01:28:03', null),
('1ac3b15c-3cca-425e-9c4a-22b869aea9c1', '22534645', 'MNG', 'NIC','2022-04-09T01:28:03', null);
/**/

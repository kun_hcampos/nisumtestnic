DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id UUID PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL,
    created TIMESTAMP NOT NULL,
    modified TIMESTAMP,
    last_login TIMESTAMP NOT NULL,
    token VARCHAR(300),
    is_active BOOLEAN NOT NULL

);

DROP TABLE IF EXISTS phones;
CREATE TABLE phones(
   id INT AUTO_INCREMENT PRIMARY KEY,
   user_id UUID NOT NULL,
   number VARCHAR(25) NOT NULL,
   city_code VARCHAR(3) NOT NULL,
   country_code varchar(3) NOT NULL,
   created TIMESTAMP NOT NULL,
   modified TIMESTAMP,
   FOREIGN KEY (user_id) REFERENCES users(id)
)